using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class TP : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (LevelAccess.Instance.Level1Unlock == true && collision.gameObject.tag == "TP1")
        {
            SceneManager.LoadScene("Level 1");
        }

        if (LevelAccess.Instance.Level2Unlock == true && collision.gameObject.tag == "TP2")
        {
            SceneManager.LoadScene("Level 2");
        }

        if (LevelAccess.Instance.Level3Unlock == true && collision.gameObject.tag == "TP3")
        {
            SceneManager.LoadScene("Level 3");
        }

    }
}

    



