using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelAccess : MonoBehaviour
{
    public bool Level1Unlock;
    public bool Level2Unlock;
    public bool Level3Unlock;
    private static LevelAccess _instance;
    
     public static LevelAccess Instance { get { return _instance; } }

    private void Awake()
    { 
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
            DontDestroyOnLoad(transform.gameObject);
        }
    }







}


